# Load tests using JMeter

## Pre-requisites
1. Download JMeter from this [page](https://jmeter.apache.org/download_jmeter.cgi)
2. Edit file `/bin/user.properties` and set `jmeter.reportgenerator.overall_granularity=1000`

## How to run
1. Go to `/bin` directory
2. Run the following command:
```
jmeter -n -t <jmx file> -l <results file> -e -o <path to report directory> -Jhost=<host name>
```
3. The HTML report should be generated in the specified directory.
